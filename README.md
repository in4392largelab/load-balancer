# Load balancer
This project contains a load balancer to divide load from clients over back-end servers. Currently a client is assigned
to a random server. Note that the application requires a Java SE 11 JRE. The application takes the following command
line arguments:

1. TCP port number to use for the configuration HTTP server
2. TCP port number to use for the client serving HTTP server
3. Log file path

## Configuration API
* POST request to `/server`: Add the server that does the request to the list of active back-end servers. Note that the
application currently requires this request to be sent at least every 30 seconds. Otherwise, the server will be removed
from the list of active servers.
* GET request to `/server`: Check the status of the server that does the request. Returns `active` or `inactive` when
the server is active and inactive, respectively.
* DELETE request to `/server`: Delete the server that does the request from the list of active back-end servers. It is
preferred to explicitly send this request instead of waiting for the time-out, to prevent that clients get redirected to
already inactive servers within the time-out period.
* GET request to `/server-list`: Returns a comma-separated list of all active back-end servers.  
  
## Starting on VM
*
