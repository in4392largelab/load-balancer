package com.in4392.loadbalancer;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import spark.Service;

/**
 * Load balancer for dividing requests between back-end servers. Chooses a server randomly per request.
 */
class LoadBalancer {

    /**
     * List of all active servers
     */
    private final List<String> servers = new CopyOnWriteArrayList<>();

    /**
     * Map from server IP to the last time a heart beat was received from that server
     */
    private final Map<String, ZonedDateTime> lastConfirmed = new ConcurrentHashMap<>();

    /**
     * Starts the configuration HTTP server for adding and removing back-end servers.
     * @param port the TCP port to start to listen at
     */
    void startConfigurationServer(int port) {
        Service http = Service.ignite().port(port);
        System.out.println("Starting HTTP server for configuration at port " + port);

        // Adds the server that does the request
        http.post("/server", (req, res) -> {
            String ipToAdd = req.ip();
            if (servers.contains(ipToAdd)) {
                System.out.println("Updating server: " + ipToAdd);
            }
            else {
                servers.add(ipToAdd);
                System.out.println("Adding server: " + ipToAdd);
            }
            lastConfirmed.put(ipToAdd, ZonedDateTime.now());
            return "";
        });

        // Deletes the server that does the request
        http.delete("/server", (req, res) -> {
            String ipToDelete = req.ip();
            servers.remove(ipToDelete);
            lastConfirmed.remove(ipToDelete);
            System.out.println("Deleted server: " + ipToDelete);
            return "";
        });

        // Retrieves the status of the server that does the request
        http.get("/server", (req, res) -> {
           String ipToCheck = req.ip();
           return servers.contains(ipToCheck) ? "active" : "inactive";
        });

        // Retrieves a comma-separated list of servers
        http.get("/server-list", (req, res) -> servers.stream().reduce((s1, s2) -> s1 + "," + s2)
                .orElse(""));

        startCheckingServerActivity();
    }

    /**
     * Starts the HTTP server that communicates with clients, and divides requests among back-end servers
     * @param port the TCP port to listen at
     */
    void startClientServer(int port) {
        System.out.println("Starting HTTP server for client requests at port " + port);
        Service http = Service.ignite().port(port);

        http.get("/", (req, res) -> {
            String clientIP = req.ip();
            String redirectServer = randomServer();
            System.out.println("Using server " + redirectServer +  " for client " + clientIP);

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("http://" + redirectServer))
                    .GET()
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        });
    }

    /**
     * Checks if a server has been active recently and deletes it from the server list if not
     */
    private void removeInactiveServers() {
        for (String server : servers) {
            if (lastConfirmed.get(server).isBefore(ZonedDateTime.now().minus(30, ChronoUnit.SECONDS))) {
                lastConfirmed.remove(server);
                servers.remove(server);
                System.out.println("Removed server because of inactivity: " + server);
            }
        }
    }

    /**
     * Starts the checking of servers for inactivity
     */
    private void startCheckingServerActivity() {
        new Thread(() -> {
            try {
                //noinspection InfiniteLoopStatement
                while(true) {
                    removeInactiveServers();
                    Thread.sleep(5_000L);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }).start();
    }

    /**
     * Returns a random server from the server list
     */
    private String randomServer() {
        return servers.get(new Random().nextInt(servers.size()));
    }
}
