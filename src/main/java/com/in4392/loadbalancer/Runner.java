package com.in4392.loadbalancer;

import org.apache.commons.io.output.TeeOutputStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

class Runner {

    /**
     *
     * @param args args[0]: HTTP port number for configuration; args[1]: HTTP port number for client requests;
     *             args[2]: log file path
     */
    public static void main(String[] args) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(args[2], true);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        TeeOutputStream teeOutputStream = new TeeOutputStream(System.out, fileOutputStream);
        System.setOut(new PrintStream(teeOutputStream));

        System.out.println("Starting load balancer");
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.startConfigurationServer(Integer.parseInt(args[0]));
        loadBalancer.startClientServer(Integer.parseInt(args[1]));
    }
}
